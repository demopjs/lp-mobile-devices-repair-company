* # Frontend
* Run ```npm install``` to install latest dependencies. You should have Node.js 6.x installed
* Run ```npm run build``` to build static templates
* Run ```npm run build:production``` to build static templates with production options like minification
* Build will be bundled in ```./build```

