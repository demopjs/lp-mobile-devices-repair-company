const 
  $    = require('gulp-load-plugins')(),
  gulp = require('gulp'),
  path = require('path'),
  argv = require('yargs').argv;

const config = require('./build.config.js');  

// Lazy task initializer
const lazyTask = function(taskName, path, options) {
  if(!options) {
    let options = {};
  }
  options.taskName = taskName;
  options.flags = config.flags;
  gulp.task(taskName, function(callback) {
    let task = require(path).call(this, options);
    return task(callback);
  });
};  


// SERVER
const localIP = argv.ip ? argv.ip : null;
// wite livedeload
lazyTask('server', './tasks/server', {
  serve : config.dir.build,
  ip : localIP
});
// with livedeload and HMR
// lazyTask('server', './tasks/server', Object.assign({}, {
//   serve : config.dir.build,
//   ip    : localIP
// }));

// CLEAN 
lazyTask('clean', './tasks/clean', {
  src : [config.dir.build, config.dir.tmp]
});

lazyTask('fonts', './tasks/assets', {
  src : config.paths.fonts.src,
  dest : config.paths.fonts.dest,
  base: config.dir.fonts
});


// ASSETS
lazyTask('img:design', './tasks/images', {
  src  : config.paths.img.src,
  dest : config.paths.img.dest,
  base : config.dir.dsgn,
});
lazyTask('img:pic', './tasks/images', {
  src : config.paths.pic.src,
  dest : config.paths.pic.dest,
  base : config.dir.pic,
});
lazyTask('svg:assets', './tasks/svg', {
  src  : config.paths.svg.src,
  dest : config.paths.svg.dest,
  base : config.dir.svg,
  tmp  : config.dir.tmp
});
lazyTask('svg:sprite', './tasks/svg-sprite', {
  src  : [
    config.paths.svg.src,
    config.paths.svgSprite.src
  ],
  // dest : config.paths.svgSprite.dest,
  dest : path.join(config.dir.tmp),
  base : config.dir.svgSprite,
  cssTemplate : config.paths.svgSprite.templates.svg,
  svgTemplate : config.paths.svgSprite.templates.css,
  tmp  : config.dir.tmp
});
// lazyTask('svg:demo', './tasks/svg-demo', {
//   src        : config.dir.svg,
//   outputFile : path.join(config.dir.tmp, 'templates/svg-demo.html'),
// });


// SCRIPTS
lazyTask('js', './tasks/scripts', config);
// lazyTask('js', './tasks/assets', {
//   src : path.join(config.dir.js, 'app.js'),
//   dest: path.join(config.dir.build, 'js'),
//   base: config.dir.js
// });


// STYLES 
let stylesGlob = argv.styles ? path.join(config.dir.stl, argv.styles) + '.styl' : config.paths.styles.src;
lazyTask('styles:raw', './tasks/styles', {
  src : stylesGlob,
  base : config.dir.stl,
  dest : config.paths.styles.dest,
});


// TEMPLATES
let templatesGlob = argv.templates ? path.join(config.dir.tpl, argv.templates) + '.pug' : config.paths.templates.src;
lazyTask('templates:raw', './tasks/templates', {
  src  : templatesGlob,
  base : config.dir.tpl,
  data : config.paths.templates.data,
  dest : config.paths.templates.dest,
});
lazyTask('templates:mixins', './tasks/collectFilesContent', {
  dir        : [
    path.join(config.dir.tpl, 'components'),
    path.join(config.dir.tpl, 'ui'),
    path.join(config.dir.tpl, 'sections'),
  ],
  outputFile : path.join(config.dir.tmp, 'templates/mixins.pug'),
  append     : '\n\n',
});

gulp.task('watch:templates', () => {
  gulp.watch(config.paths.templates.watch)
    .on('all', (event, path) => {
      if(path.indexOf('sections') >= 0 
          || path.indexOf('ui') >= 0
          || path.indexOf('components') >= 0) {
        gulp.series('templates:mixins', 'templates:raw')();
      }
      else {
        gulp.series('templates:raw')();
      }
    });
});


// COMMON
gulp.task('img', gulp.parallel('img:design', 'img:pic'));
gulp.task('svg', gulp.series('svg:assets', 'svg:sprite'));
gulp.task('styles', gulp.series('styles:raw'));
gulp.task('templates', gulp.series('templates:mixins', 'templates:raw'));


// WATCH
gulp.task('watch:main', function () {
  if(config.flags.shouldWatch) {
    gulp.series('watch:templates')();
    gulp.watch(config.paths.styles.watch, gulp.series('styles:raw'));
  }
});
gulp.task('watch:assets', () => {
  if(config.flags.shouldWatch) {
      gulp.watch(config.paths.img.watch, gulp.series('img:design'));
      gulp.watch(config.paths.svg.watch, gulp.series('svg'))
        .on('unlink', function(filepath) {
          $.remember.forget('svg', path.resolve(filepath));
          delete $.cached.caches.svg[path.resolve(filepath)];
        });
  }
});
gulp.task('watch', gulp.parallel('watch:main', 'watch:assets'));


// GENERAL 
gulp.task('build:main', gulp.series('svg', gulp.parallel('styles', 'templates', 'js')));
gulp.task('build:assets', gulp.series('img', 'fonts'));
gulp.task('build', gulp.parallel('build:main', 'build:assets'));


gulp.task('dev', gulp.series('clean', 'build', gulp.parallel('watch', 'server')));
gulp.task('frontend', gulp.series(gulp.parallel('build:main', 'build:assets'), gulp.parallel('watch', 'server')));

