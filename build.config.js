'use strict';

const path = require('path');

let dir = {
  root : './'
};

// main dirs
dir.src    = path.join(dir.root, 'source-files');
dir.assets = path.join(dir.src, 'assets');
dir.build  = path.join(dir.root, 'build');
dir.tmp    = path.join(dir.root, 'tmp');

// src dirs
dir.stl       = path.join(dir.src, 'styles');
dir.tpl       = path.join(dir.src, 'templates');
dir.js        = path.join(dir.src, 'scripts');
dir.fonts     = path.join(dir.assets, 'fonts');
dir.pic       = path.join(dir.assets, 'pictures');
dir.dsgn      = path.join(dir.assets, 'images');
dir.svg       = path.join(dir.assets, 'svg');
dir.svgSprite = path.join(dir.assets, 'svg-sprite');

let flags = {};
flags.mode        = process.env.NODE_ENV || 'development';
flags.isDev       = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';
flags.isProd      = process.env.NODE_ENV == 'production';
flags.debug       = process.env.DEBUG == 'true';
flags.shouldWatch = !process.env.WATCH || process.env.NODE_WATCH == 'true';

module.exports = {
  dir, flags,
  paths : {
    js : {
      context    : process.cwd(),
      entry      : path.join(process.cwd(), dir.js, 'app'),
      output     : path.resolve('./', dir.build, 'js'),
      publicPath : '/js/',
      watch      : path.join(dir.js, '**/*.js@(|x|on)'),
    },
    styles: {
      watch : path.join(dir.stl, '**/*.*'),
      src   : path.join(dir.stl, '!(_)*.styl'),
      dest  : path.join(dir.build, 'css'),
    },
    templates : {
      data  : path.join(dir.tpl, 'data'),
      watch : path.join(dir.tpl, '**/*.*'),
      src   : path.join(dir.tpl, '!(_)*.@(pug|jade)'),
      dest  : dir.build,
    },
    fonts : {
      src  : path.join(dir.fonts, '**.*'),
      dest : path.join(dir.build, 'fonts'),
    },
    img : {
      watch : path.join(dir.dsgn, '**/*.@(jpg|jpeg|png|gif|svg)'),
      src   : path.join(dir.dsgn, '**/*.@(jpg|jpeg|png|gif|svg)'),
      dest  : path.join(dir.build, 'img'),
    },
    pic : {
      watch : path.join(dir.pic, '**/*.@(jpg|jpeg|png|gif|svg)'),
      src   : path.join(dir.pic, '**/*.@(jpg|jpeg|png|gif|svg)'),
      dest  : path.join(dir.build, 'pic'),
    },
    svg : {
      watch : path.join(dir.svg, '**/*.svg'),
      src   : path.join(dir.svg, '**/*.svg'),
      dest  : path.join(dir.build, 'img'),
    }, 
    svgSprite : {
      watch : path.join(dir.svgSprite, '**/*.svg'),
      src   : path.join(dir.svgSprite, '**/*.svg'),
      dest  : path.join(dir.build, 'img'),
      templates : {
        svg : path.resolve(dir.assets, 'svg-sprite-templates/svg-sprite.svg'),
        css : path.resolve(dir.assets, 'svg-sprite-templates/svg-sprite.css'),
      },
    },  
  }
  
};
