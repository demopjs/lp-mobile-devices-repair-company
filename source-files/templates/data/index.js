module.exports = { 
  phones: [
    {
      icon: 'handset',
      number: '8 800 200-50-78',
      description: 'Звоните в любое время!',
      href: 'tel:88002005078',
    },
    {
      icon: 'paper-plane',
      number: '8 906 747 76 96',
      description : 'Пишите нам в Telegram',
      href: 'https://telegram.me/lcd1ru',
    },
  ],

  features: [
    {
      icon: 'feature-heart',
      text: 'Бережное отношение к клиенту'
    },
    {
      icon: 'feature-master',
      text: 'Неравнодушные мастера'
    },
    {
      icon: 'feature-gear',
      text: 'Качественные запчасти'
    },
    {
      icon: 'feature-car',
      text: 'Бесплатный выезд в удобное время и место'
    },
    {
      icon: 'feature-payment',
      text: 'Оплата картой'
    },
    {
      icon: 'feature-shield',
      text: 'Гарантия на работы'
    },
  ],

  prizes : [
    {
      image: '1',
      title : 'Вы выграли защитное стекло!',
    },
    {
      image: '2',
      title : 'Вы выграли зарядку для телефона!'
    },
    {
      image: '3',
      title : 'Вы выграли MP3-плеер!'
    },
  ]
};