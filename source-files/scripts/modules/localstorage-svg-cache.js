export default class LoadSVG {
  
  constructor(options) {

    this.id       = options.id || 'null';
    this.itemName = this.id + '-inlineSVGdata';

    this.path     = options.path;
    this.debug    = options.debug;
    this.revision = 1;

    if(!document.createElementNS || 
      !document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect) {
      return false;
    }

    this.isLocalStorage = 'localStorage' in window && window['localStorage'] !== null;

    this.insert   = this.insert.bind(this);
    this.insertIT = this.insertIT.bind(this);
    this.init     = this.init.bind(this);

    this.init();
    
  }

  init() {
    if(this.isLocalStorage && 
      localStorage.getItem('inlineSVGrev') == this.revision && 
    !this.debug) {
        this.data = localStorage.getItem(this.itemName);
        if(this.data) {
          this.insert();
          return true;
        }
    }
    try {
      let request = new XMLHttpRequest();
      request.open('GET', this.path, true);
      request.onload = () => {
        if(request.status >= 200 && request.status < 400) {
          this.data = request.responseText;
          this.insert();
          if(this.isLocalStorage) {
            localStorage.setItem(this.itemName, this.data);
            localStorage.setItem('inlineSVGrev', this.revision);
          }
        }
      };
      request.send();
    }
    catch(e) {
      console.error(`Could not get SVG content. ${e}`);
    }
  }

  insertIT() {
    try {
      document.body.insertAdjacentHTML('afterbegin', this.data);
    }
    catch(e) {
      console.error(`Could not insert SVG content into the page. ${e}`);
    }
  }

  insert() {
    if(document.body) {
      this.insertIT();
    }
    else {
      document.addEventListener('DOMContentLoaded', this.insertIT);
    }
  }


}