const PRICING_EVENT = 'updatePricing';

const triggerPricingEvent = (data = {}) => {
  if(!data.nav) data.nav = 0;
  if(!data.subnav) data.subnav = 0;
  $(document).trigger(PRICING_EVENT, data);
};

const init = () => {

  const 
    $pricingNavItems = $('[data-pricing-nav-item]'),
    $pricingSubnavs = $('[data-pricing-subnav]'),
    $pricingSubnavItems = $('[data-pricing-subnav-item]'),
    $pricingSubnavSelects = $('[data-pricing-subnav-select]'),
    $pricingSubnavContents = $('[data-pricing-subnav-content]');

  $pricingNavItems.on('click', function(e) {
    triggerPricingEvent({
      nav: $(this).index() || 0,
      subnav: 0
    });
  });

  $pricingSubnavItems.on('click', function(e) {
    triggerPricingEvent({
      nav: $pricingNavItems.filter('.is-active').index() || 0,
      subnav: $(this).index() || 0,
    });
  });

  $pricingSubnavSelects.on('change', function(e) {
    triggerPricingEvent({
      nav: $pricingNavItems.filter('.is-active').index() || 0,
      subnav: e.target.value || 0,
    });
  });


  $(document).on(PRICING_EVENT, function(e, data) {
    
    $pricingNavItems
      .eq(data.nav).addClass('is-active')
      .siblings().removeClass('is-active');

    const $currentSubnav = $pricingSubnavs.eq(data.nav);
    $currentSubnav.addClass('is-active').siblings().removeClass('is-active');
    $currentSubnav.children().removeClass('is-active')
      .eq(data.subnav).addClass('is-active');

    $pricingSubnavSelects
      .removeClass('is-active')
      .eq(data.nav).addClass('is-active').find('select').val(data.subnav);
    
    $pricingSubnavContents
      .removeClass('is-active')
      .filter(function() { return $(this).attr('data-parent-nav') === data.nav.toString()})
      .filter(function() { return $(this).attr('data-parent-subnav') === data.subnav.toString()})
      .addClass('is-active');

  });

};

export default init();