import debounce from 'lodash/debounce';
import each from 'lodash/each';

const dataMock = require('../../templates/data/example');
const PRICING_EVENT = 'updatePricing';


const init = () => {

  const 
    $inputs = $('[data-sync-input]'),
    $modelInputs = $('input[name="model"]');


  const updateInputs = (e) => {
    let 
      $this = $(e.target),
      value = e.target.value,
      name = $this.attr('name');
    let $siblings = $inputs.filter(function() { 
        if(name == $(this).attr('name'));
        return (name == $(this).attr('name') && $(this)[0] != $this[0]);
      });
    $siblings.val(value);
  };

  $inputs.on('keyup', debounce(updateInputs, 500));

  $(document).on(PRICING_EVENT, function(e, data) {
    let 
      deviceName = Object.keys(dataMock)[data.nav],
      deviceModel;
    each(dataMock[deviceName], (value, key) => {
      if(key == data.subnav) deviceModel = Object.keys(value)[0];
    });
    $modelInputs.val(deviceName + ' ' + deviceModel);
  });

};

export default init();