const showOverlay = ($overlay) => {
  $overlay.css('display', 'flex');
};
const hideOverlay = ($overlay) => {
  $overlay.css('display', 'none');
};

const init = () => {
  const 
    // $submitButtons = $('[data-submit-button]'),
    $forms = $('[data-form]'),
    $overlay = $('[data-overlay]'),
    $closeOverlay = $('[data-close-overlay]');

  $closeOverlay.on('click', () => { hideOverlay($overlay) });
  $overlay.on('click', (e) => {
    const $target = $(e.target);
    if(!$target.parents('.modal').length && !$target.hasClass('modal')) {
      hideOverlay($overlay);
    }
  });

  // $submitButtons.on('click', function() {
  //   $(this).parents('[data-form]').submit();
  // });

  $forms.on('submit', function(e) {
    e.preventDefault();
    const 
      $form = $(this),
      formData = $(e.target).serialize(),
      url = $form.attr('action');

    $.ajax(url, {
      data        : formData,
      type        : 'post',
      processData : false
    })
      .done(function() {
        showOverlay($overlay);
      })
      .fail(function(res) {
        if(res && res.responseText && res.responseText.errors) {
          alert('Не удалось отправить заявку. Ошибка: ' + res.responseText.errors.toString());  
        }
        else {
          alert('Не удалось отправить заявку.');
        }
      });

    showOverlay($overlay);
        
    return false;
  });

};

export default init();