const init = () => {

  const 
    $choosePrizeBox = $('[data-choose-prize-box]'),
    $choosePrizeButtons = $('[data-choose-prize]'),
    $prizeBox = $('[data-the-prize-box]'),
    date = new Date().getTime();


  const setLocalstorageData = index => {
    localStorage.setItem('prizeIndex', index);
    localStorage.setItem('prizeSelectedAt', new Date().getTime());
  };
  const choosePrize = index => {
    $choosePrizeBox.removeClass('is-active');
    $prizeBox.addClass('is-active')
      .children().removeClass('is-active')
      .eq(index).addClass('is-active');
    setLocalstorageData(index);
  };

  const 
    existingPrize = localStorage.getItem('prizeIndex'),
    existingTimestamp = localStorage.getItem('prizeSelectedAt');
  if(existingPrize && existingTimestamp) {
    if(date - parseInt(existingTimestamp) < 1000*60*60*24) {
      choosePrize(parseInt(existingPrize));
    }
  }

  $choosePrizeButtons.on('click', function() {
    const index = $(this).index();
    choosePrize(index);
  });

};

export default init();