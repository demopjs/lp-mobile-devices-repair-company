'use strict';

const
  path        = require('path'),
  debounce    = require('lodash/debounce'),
  browserSync = require('browser-sync').create();

module.exports = function(options) {
  return function() {
    browserSync.init({
      server: {
        baseDir   : options.serve,
        directory : true,
        // index     : 'index.html',
      },
      host      : options.ip || '192.168.1.105',
      open      : false,
      notify    : false,
      logLevel  : 'info',
      logPrefix : 'BrowserSync',
      online    : true, // increases startup time 
    });
    browserSync.watch(path.join(options.serve, '**/*.*'))
      .on('change', debounce(browserSync.reload, 500));
  };
};