'use strict';

const 
  path   = require('path'),
  mkpath = require('mkpath'),
  fs     = require('fs');

module.exports  = function(options) {

  return function(callback) {

    let dirs = [];

    if(typeof options.dir === 'string') {
      dirs.push(options.dir);
    } else {
      dirs = options.dir;
    }

    return Promise.all(dirs.map(dirPath => {
      return new Promise(function(resolve, reject) {
        fs.readdir(path.resolve(process.cwd(), dirPath), function(err, files) {
          if(err) {
            reject(err);
          } else {
            resolve(files.map(filePath => {
              return path.join(dirPath, filePath);
            }));
          }
        });
      }).catch(e => {throw new Error(e)});
    }))

      .then(filePaths => {

        let fileReadPromises = [];

        filePaths.forEach(fileArray => {
          fileArray.forEach(fileName => {

            let promise = new Promise(function(resolve, reject) {
              fs.readFile(fileName, (err, data) => {
                if(err) {
                  reject(err);
                }
                else {
                  resolve(data);
                }
              })
            }).catch(e => {throw new Error(e)});

            fileReadPromises.push(promise);

          });
        });

        return Promise.all(fileReadPromises);

      })

      .then(fileContents => {
        let mixinsContent = fileContents.join(options.append);
        try {
          mkpath.sync(path.dirname(options.outputFile));
        } catch(e) {
          throw new Error(e);
        }

        return fs.writeFile(
          path.resolve(options.outputFile), 
          mixinsContent, 
          (err) => {
            if(err) {
              throw new Error(err);
            }
          });
      })

      .catch(err => {
        console.log(err);
        throw new Error(err);
      });

  };
};
