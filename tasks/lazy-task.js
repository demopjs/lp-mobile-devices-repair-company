const path = require('path');

module.exports = function(gulp, config = {}) {
  return (taskName, taskPath, options = {}) => {
    options.taskName = taskName;
    options.flags = config.flags;
    gulp.task(taskName, function(callback) {
      let task = require(path.resolve(process.cwd(), taskPath)).call(this, options);
      return task(callback);
    });
  };
};

