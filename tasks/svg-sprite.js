const 
  gulp     = require('gulp'),
  path     = require('path'),
  combiner = require('stream-combiner2').obj,
  $        = require('gulp-load-plugins')();

module.exports = function(options) {
  return function() {
    return combiner(
      gulp.src(options.src, { base: options.base }),

      $.svgSymbols({
        id        : 'svg-icon_%f',
        className : '.svg-icon_%f',
        templates : [options.cssTemplate, options.svgTemplate],
        title     : false,
      }),
        $.if(options.flags.debug, 
          $.debug({title : 'DEBUG ' + options.taskName + ':processed'})),
      
      $.cheerio({
        run : ($$, file) => {
          $$('[fill]').removeAttr('fill');
          $$('title, desc, defs').remove();
          $$('symbol').each(function() {
            $$(this).find('path, polygon, ellipse').each(function(i) {
              $$(this).addClass(`svg-icon-path-${i+1}`);
            });
          });
        }
      }),
        
      $.if('*.{css,sass,scss,less,styl}', 
        gulp.dest(path.join(options.tmp, 'styles')), 
        gulp.dest(options.dest))
    ).on('error', $.notify.onError(function(err) {
      console.log(err);
        return {
          title : 'SVG',
          message: err.message
        };
    }));  
  };
};