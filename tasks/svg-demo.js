'use strict';

const 
  path   = require('path'),
  mkpath = require('mkpath'),
  fs     = require('fs');

const renderItem = (name) => {
  return `
    <div class="svg-demo__item">
      <svg fill="blue" class="svg_icon svg-demo__item__icon svg-icon_${name}">
        <use 
          xmlns:xlink="http://www.w3.org/1999/xlink" 
          xlink:href = "#svg-icon_${name}" />
      </svg>
      <span class="svg-demo__item__name">${name}</span>
    </div>
  `;
};

module.exports = function(options) {
  return function(callback) {

    return new Promise((resolve, reject) => {
      fs.readdir(options.src, (err, files) => {
        if(err) {
          reject(err);
        } else {
          resolve(files);
        }
      });
    })
      .then(filesList => {
        return filesList.map(item => {
          return renderItem(item.replace('.svg', ''));
        }).join('');
      })
      .then(demoPageString => {
        try {
          mkpath.sync(path.dirname(options.outputFile));
        } catch(e) {
          throw new Error(e);
        }
        
        return fs.writeFile(
          path.resolve(options.outputFile), 
          demoPageString, 
          (err) => {
            if(err) {
              throw new Error(err);
            }
          });
        
      })
      .catch(e => e);

  };
};
