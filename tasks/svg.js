const 
  gulp     = require('gulp'),
  path     = require('path'),
  combiner = require('stream-combiner2').obj,
  $        = require('gulp-load-plugins')();

module.exports = function(options) {
  return function() {
    return combiner(
      gulp.src(options.src, { base: options.base }),

      $.cached('svg'),
        $.if(options.flags.debug, $.debug({title : 'DEBUG ' + options.taskName + ':src'})),
        
      $.remember('svg'),
        $.if(options.flags.debug, $.debug({title : 'DEBUG ' + options.taskName + ':remembered'})),
      
      $.cheerio({
        run : ($$, file) => {
          $$('svg').addClass(`svg-img_${file.stem}`);
          $$('svg').css('fill', 'currentColor');
          $$('title, desc').remove();
        }
      }),
        
      $.if('*.{css,sass,scss,less,styl}', 
        gulp.dest(path.join(options.tmp, 'styles')), 
        gulp.dest(options.dest))
      
    ).on('error', $.notify.onError(function(err) {
      console.log(err);
        return {
          title : 'SVG',
          message: err.message
        };
    }));  
  };
};