'use strict';

const 
  browserSync          = require('browser-sync').create(),
  webpackDevMiddleware = require('webpack-dev-middleware'),
  webpackHotMiddleware = require('webpack-hot-middleware'),
  path                 = require('path'),
  debounce             = require('lodash/debounce'),
  gulplog              = require('gulplog'),
  webpack              = require('webpack'),
  notifier             = require('node-notifier');

const webpackConfig = require('../../frontend/webpack.config.js');

module.exports = function(config) {

  if(config.flags.isProd) {
    webpackConfig.plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compress : {
          warnings     : false,
          drop_console : true,
          unsafe       : true,
        }
      })
    );
  }

  if(config.flags.isDev) {
    webpackConfig.plugins.unshift(
      // new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.HotModuleReplacementPlugin()
    );
    webpackConfig.debug = true;
    webpackConfig.entry = [
      'react-hot-loader/patch',
      'webpack/hot/dev-server',
      'webpack-hot-middleware/client',
      config.paths.js.entry,
    ]
  }

  const webpackBundler = webpack(webpackConfig);

  
  const init = () => {
    browserSync.init({
      open      : false,
      notify    : false,
      logLevel  : 'info',
      logPrefix : 'BrowserSync',
      server: {
        baseDir: config.serve,
        directory : false,
        // host      : config.ip || null,
        middleware : [
          webpackDevMiddleware(webpackBundler, {
            publicPath: webpackConfig.output.publicPath,
            stats : {
              colors       : true,
              hash         : config.flags.debug,
              cached       : config.flags.debug,
              cachedAssets : config.flags.debug,
              chunkOrigins : config.flags.debug,
              chunkModules : config.flags.debug,
            }
          }),
          webpackHotMiddleware(webpackBundler),
        ]
      }
    });
    // browserSync.watch(path.join(config.serve, '**/*.@(html|css)'))
    //   .on('change', debounce(browserSync.reload, 500));
  }
  

  return function(callback) {
    init();
  };

};
