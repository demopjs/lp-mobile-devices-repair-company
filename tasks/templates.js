'use strict';

const 
  gulp     = require('gulp'),
  combiner = require('stream-combiner2').obj,
  path     = require('path'),
  fs       = require('fs'),
  $        = require('gulp-load-plugins')();

let dataObject = null, timer = null, dataFilesList = [];

const getTemplatesData = function(dataPath) {
  // we clear timer because we want in to run a callback
  // only on the very last file in the stream
  if(timer) clearTimeout(timer);
  
  // normally dataObject will be 'null' only for 
  // the very first file in the stream
  if(!dataObject) {
    let dirPath = path.join(process.cwd(), dataPath);
    dataObject = {};
    dataFilesList = fs.readdirSync(dirPath);
    dataFilesList
      .map(item => item.replace('.js', ''))
      .forEach(item => {
        let itemPath = path.join(dirPath, item);
        // now we delete cache and require the data
        delete require.cache[require.resolve(itemPath)];
        dataObject[item] = require(itemPath);
      });
  }
  
  // we clear dataObject so it will be empty when 
  // the next gulp templates task will run
  timer = setTimeout(function(){ 
    dataObject = null;
  }, 1000);
  return { data: dataObject };
};

module.exports = function(options) {
  return function() {
    return combiner(
      gulp.src(options.src, { base: options.base }),
        $.if(options.flags.debug, $.debug({title : 'DEBUG ' + options.taskName + ':src'})),

      $.pug({
        pretty: true,
        // debug: true,
        // filters: {},
        cache: true,
        data : getTemplatesData(options.data)
      }),
        $.if(options.flags.debug, $.debug({title : 'DEBUG ' + options.taskName + ':compiled'})),
      
      gulp.dest(options.dest),
        $.if(options.flags.debug, $.debug({title : 'DEBUG ' + options.taskName + ':dest'}))      

    ).on('error', $.notify.onError(function(err) {
        return {
          title : 'Templates',
          message: err.message
        };
    }));  
  };
};